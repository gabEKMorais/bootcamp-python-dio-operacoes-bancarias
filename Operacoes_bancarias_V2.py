# parametros
saldo = 0
depositos = []
LIMITE_SAQUE_DIARIO = 3
saques = []
saques_realizados = 0
clientes_cadastrados = []
contas_bancarias = []

# classes
class cliente:
    def __init__(self,nome,data_nascimento,cpf,endereco):
        self.nome = nome
        self.data_nascimento = data_nascimento
        self.cpf = cpf
        self.endereco = endereco
        print("Cadastrado com sucesso!")
class conta:
    global contas_bancarias
    global clientes_cadastrados
    def __init__(self, id_usuario):
        self.id_usuario = id_usuario
        self.agencia = "0001"
        self.conta = (len(contas_bancarias)+1)
        print(f"Conta {self.conta} aberta para o usuario {clientes_cadastrados[id_usuario].nome}!")
        
# funções - operacoes
def deposito (valor):
    global saldo
    if (valor > 0):
        saldo  += valor
        print(f"Saldo atualizado, valor atual e  R${saldo:.2f}")
        return 1
    else:
        print("Valor nao permitido!")
        return 0
def saque(valor):
    global saldo
    if (valor > 0):
        if (saldo >= valor):
            saldo -= valor
            print(f"Saldo atualizado, valor atual e R${saldo:.2f}")
            return 1
        else:
            print("Saldo insuficiente!")
            return 0
    else:
        print("Valor nao permitido!")
        return 0
def extrato():
    global saldo
    if (len(depositos) == 0 and len(saques) == 0):
        print("Nao foram realizadas movimentacoes!")
        return
    if (len(depositos) > 0):
        print("Depositos realizados:")
        cont_op = 1
        for op in depositos:
            print(f"A operacao {cont_op} no valor de R${op:.2f}")
            cont_op += 1
    if (len(saques) > 0):
        print("Saques realizados:")
        cont_op = 1
        for op in saques:
            print(f"A operacao {cont_op} no valor de R${op:.2f}")
            cont_op += 1
    print(f"Saldo atual R${saldo:.2f}")
    return
# Funções do cadastro de usuario e conta
def cadastra_conta(cpf):
    global clientes_cadastrados
    global contas_bancarias
    id_usuario = 0
    for cliente in clientes_cadastrados:
        if(cpf == cliente.cpf):
            return contas_bancarias.append(conta(id_usuario))
        id_usuario += 1
    print("Cpf nao localizado!")
    return

# Programa
print("Bem-vindo")
while(True):
    print("----------------------------------------------------")
    print("Operacoes disponiveis:")
    print("1 - Deposito")
    print("2 - Saque")
    print("3 - Extrato")
    print("4 - Cadastrar cliente")
    print("5 - Cadastrar conta")
    print("6 - Sair")
    operacao = int(input("Qual operacao deseja? "))
    if (operacao == 1):
        print("Digite o valor para o deposito!")
        valor = float(input("Valor: "))
        if (deposito(valor) == 1):
            depositos.append(valor)
    elif (operacao == 2 and saques_realizados != LIMITE_SAQUE_DIARIO):
        print("Digite o valor para o saque!")
        valor = float(input("Valor: "))
        while(valor > 500):
            print("Limite por saque e R$500,00")
            print("Escolha um novo valor! 0 (zero) para cancelar o saque")
            valor = input("Valor: ")
        if (valor == 0):
            continue
        if (saque(valor) == 1):
            saques.append(valor)
            saques_realizados += 1
    elif (operacao == 2 and saques_realizados == LIMITE_SAQUE_DIARIO):
        print("Atingido limite de saques diarios")
    elif (operacao == 3):
        extrato()
    elif (operacao == 4):
        op_nome = input("Nome: ")
        op_data_nascimento = int(input("Data de nascimento (DDMMAAAA): "))
        op_cpf = input("CPF: ")
        op_endereco = input("Endereco: ")
        clientes_cadastrados.append(cliente(op_nome,op_data_nascimento,op_cpf,op_endereco))
    elif (operacao == 5):
        cadastra_conta(input("CPF: "))
    elif (operacao == 6):
        break
    else:
        print("Operacao invalida")
    input("Enter para continuar..")
