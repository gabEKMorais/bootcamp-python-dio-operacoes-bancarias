'''
Neste desafio você deverá implementar três operações:
depósito, saque e extrato
para um banco que deseja monetizar suas operações em Python.
'''
# parametros
saldo = 0
depositos = []
LIMITE_SAQUE_DIARIO = 3
saques = []
saques_realizados = 0
# funções - operacoes
def deposito (valor):
    global saldo
    if (valor > 0):
        saldo  += valor
        print("Saldo atualizado, valor atual e  R$%.2f" % (saldo))
        return 1
    else:
        print("Valor nao permitido!")
        return 0
def saque(valor):
    global saldo
    if (valor > 0):
        if (saldo >= valor):
            saldo -= valor
            print("Saldo atualizado, valor atual e R$%.2f" % (saldo))
            return 1
        else:
            print("Saldo insuficiente!")
            return 0
    else:
        print("Valor nao permitido!")
        return 0
def extrato():
    global saldo
    if (len(depositos) == 0 and len(saques) == 0):
        print("Nao foram realizadas movimentacoes!")
        return
    if (len(depositos) > 0):
        print("Depositos realizados:")
        cont_op = 1
        for op in depositos:
            print("A operacao %d no valor de R$%.2f" % (cont_op,op))
            cont_op += 1
    if (len(saques) > 0):
        print("Saques realizados:")
        cont_op = 1
        for op in saques:
            print("A operacao %d no valor de R$%.2f" % (cont_op,op))
            cont_op += 1
    print("Saldo atual R$%.2f" % (saldo))
    return

# Programa
print("Bem-vindo")
while(True):
    print("----------------------------------------------------")
    print("Operacoes disponiveis:")
    print("1 - Deposito")
    print("2 - Saque")
    print("3 - Extrato")
    print("4 - Sair")
    operacao = int(input("Qual operacao deseja? "))
    if (operacao == 1):
        print("Digite o valor para o deposito!")
        valor = float(input("Valor: "))
        if (deposito(valor) == 1):
            depositos.append(valor)
    elif (operacao == 2 and saques_realizados != LIMITE_SAQUE_DIARIO):
        print("Digite o valor para o saque!")
        valor = float(input("Valor: "))
        while(valor > 500):
            print("Limite por saque e R$500,00")
            print("Escolha um novo valor! 0 (zero) para cancelar o saque")
            valor = float(input("Valor: "))
        if (valor == 0):
            continue
        if (saque(valor) == 1):
            saques.append(valor)
            saques_realizados += 1
    elif (operacao == 2 and saques_realizados == LIMITE_SAQUE_DIARIO):
        print("Atingido limite de saques diarios")
    elif (operacao == 3):
        extrato()
    elif (operacao == 4):
        break
    else:
        print("Operacao invalida")